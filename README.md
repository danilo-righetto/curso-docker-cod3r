# Curso-docker

## How Dev

### Dependencies

1. Docker
2. Docker Compose

#### How Setup docker

Run this commands:

1. `sudo groupadd docker`
2. `sudo chown root:docker /var/run/docker.sock`
3. `sudo usermod -a -G docker $USERNAME`
4. `sudo systemctl enable docker`

#### Aulas

##### Hello Docker

``` sh
docker container run hello-world
```

> Se o comando acima executar corretamente
> significa que o docker foi instalado corretamente

##### Ferramentas Diferentes

Verificando a versão do `bash` do seu computador:
``` sh
bash --version
```

Verificando a versão do `bash` do `container`:
``` sh
docker container run debian bash --version
```

Verificando os `containers` ativos:
``` sh
docker container ps
```

Verificando os `containers` ativos e inativos:
``` sh
docker container ps -a
```

Executando um `container` e removendo o mesmo após a execução:
``` sh
docker container run --rm debian bash --version
```

##### Run cria sempre novos containers

Executando um `container` habilitando o terminal de modo iteratvo:
``` sh
docker container run -it debian bash
```

##### Containers devem ter nomes únicos

Verificando os comandos disponíveis do `docker container run`:
``` sh
docker container run --help
```

Atribuindo um nome á um novo `container`:
``` sh
docker container run --name mydeb -it debian bash
```

> Não se pode ter mais de um container
> com o mesmo nome

##### Reutilizar containers

Listando todos os `containers` que foram criados:
``` sh
docker container ls -a
```

Criando um `container` de modo iterativo (anexando o terminal do container):
``` sh
docker container start -ai mydeb
```

> Usando esse comando é possível modificar os
> dados presentes dentro do `container` e ao
> executar novamente verificar que as 
> mudanças permaneceram dentro do `container`

##### Mapear portas dos containers

``` sh
docker container run -p 8080:80 nginx
```

##### Mapear diretórios para o container (volumes)

Para executar esse domando use a pasta `ex-volume`:
``` sh
docker container run -p 8080:80 -v $(pwd)/html:/usr/share/nginx/html nginx
```

> `$(pwd)/html` esse comando vai pegar a pasta
> `html` que esta dentro da pasta atual onde
> este comando foi executado

##### Rodar um servidor web em background

Para executar o `container` em background use o comando:
``` sh
docker container run -d --name ex-daemon-basic -p 8080:80 -v $(pwd)/html:/usr/share/nginx/html nginx
```

Para parar o `container` use o comando abaixo:
``` sh
docker container stop ex-daemon-basic
```

##### Gerenciar o container em background

Para inciar o `container` use o comando abaixo:
``` sh
docker container start ex-daemon-basic
```

Para reinciar o `container` use o comando abaixo:
``` sh
docker container restart ex-daemon-basic
```

Para parar o `container` use o comando abaixo:
``` sh
docker container stop ex-daemon-basic
```

##### Manipulando containers em modo daemon

Para ver os logs do `container` use o comando abaixo:
``` sh
docker container logs ex-daemon-basic
```

Para ver as informações do `container` use o comando abaixo:
``` sh
docker container inspect ex-daemon-basic
```

Para executar comandos dentro do `container` use o comando abaixo:
``` sh
docker container exec ex-daemon-basic uname -or
```

##### Nova sintaxe do Docker Client

Para listar os `containers` use o comando abaixo:
``` sh
docker container ls
```

Para listar as `images` use o comando abaixo:
``` sh
docker image ls
```

Para listar os `volumes` use o comando abaixo:
``` sh
docker volume ls
```

> Removendo

Para remover os `containers` use o comando abaixo:
``` sh
docker container rm `id-do-container`
```

Para remover as `images` use o comando abaixo:
``` sh
docker image rm `id-da-imagem`
```

Para remover os `volumes` use o comando abaixo:
``` sh
docker volume rm `id-do-volume`
```